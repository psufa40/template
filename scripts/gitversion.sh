#!/bin/bash

echo "Check project version variable exist..."
CHECK=$(curl -k --header "PRIVATE-TOKEN: $CI_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/variables/BUILD_VERSION")
echo "ProjectID is: "$CI_PROJECT_ID "Chec status is : "$CHECK
#Rules block
if 
[[ $RULE == "default" ]]
    then
        export RULE=default
        export DESCRIPTION="Default patch increment rule"
    else
        export RULE=minor
        export DESCRIPTION="Minor increment and patch zeroing rule"
fi

#Logic block

if [[ $CHECK == *"404 Variable Not Found"* ]]
    then
        echo $'Project have no version variablenAdding new Version variable...n'
        CREATE=$(curl -k --request POST --header "PRIVATE-TOKEN: $CI_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/variables" --form "key=BUILD_VERSION" --form "value=1.0.0")
        echo "Created standart version variable ver 1.0.0"
        echo "1.0.0" > .version.user

    else
        echo $'Project has previous version variable...'
        echo $BUILD_VERSION
        echo $"Increment rules is: $RULE Description: $DESCRIPTION"
        if
        [[ $RULE == "default" ]]
            then
                arrIN=(${BUILD_VERSION//./ })
                echo $"Increment $BUILD_VERSION pach val ${arrIN[2]} by 1"
                NEW_BUILD_VERSION=${arrIN[0]}.${arrIN[1]}.$((arrIN[2]+=1))
                echo "Create temp version file" && echo $NEW_BUILD_VERSION > .version.user
                echo "New BUILD_VERSION: $NEW_BUILD_VERSION"
                curl -k --request PUT --header "PRIVATE-TOKEN: $CI_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/variables/BUILD_VERSION" --form "value=$NEW_BUILD_VERSION" > /dev/null
            else
                arrIN2=(${BUILD_VERSION//./ })
                echo $"Increment $BUILD_VERSION minor val ${arrIN2[1]} by 1"
                NEW_BUILD_VERSION=${arrIN2[0]}.$((arrIN2[1]+=1)).0
                echo "Create temp version file" && echo $NEW_BUILD_VERSION > .version.user
                echo "New BUILD_VERSION: $NEW_BUILD_VERSION"
                curl -k --request PUT --header "PRIVATE-TOKEN: $CI_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/variables/BUILD_VERSION" --form "value=$NEW_BUILD_VERSION" > /dev/null
            fi
fi